# find Sleepycat BDB information
%define je_url %((grep /je/ %{_sourcedir}/sources || \
                  grep /je/ %{_builddir}/sources  || \
                  echo "x unknown") 2>/dev/null | perl -ane 'print("$F[1]")')
%define je_jar %(basename %{je_url})

# find Apollo information
%define apollo_url %((grep /apache-apollo/ %{_sourcedir}/sources || \
                      grep /apache-apollo/ %{_builddir}/sources  || \
                      echo "x unknown") 2>/dev/null | perl -ane 'print("$F[1]")')
%define apollo_tar %(basename %{apollo_url})
%define apollo_version %(echo %{apollo_tar} | perl -pe 's/^apache-apollo-//; s/-unix-distro\\.tar\\.gz$//')
%define trunk_version %((cat %{_sourcedir}/99-trunk-version || \
                         cat %{_builddir}/99-trunk-version  || \
                         echo UNKNOWN) 2>/dev/null)
%define is_snapshot %(echo %{apollo_version} | perl -ne 'print /-\\d{8}\\.\\d{6}-\\d+$/ ? 1 : 0')

# deduce rpm and tar information
%if %{is_snapshot}
%define rpm_release %(echo %{apollo_version} | perl -pe 's/.+-(\\d{8}\\.\\d{6})-(\\d+)$/0.$1.$2/')
%define rpm_version %(echo %{apollo_version} | perl -pe 's/-\\d{8}\\.\\d{6}-\\d+$//')
%if "x%{rpm_version}" == "x99-trunk"
%define rpm_version %{trunk_version}
%define tar_version 99-trunk-SNAPSHOT
%else
%define tar_version %{rpm_version}-SNAPSHOT
%endif
%else
%define rpm_release 1
%define rpm_version %{apollo_version}
%define tar_version %{apollo_version}
%endif

# installation settings
%define apollo_share /usr/share/apollo
%define apollo_home  %{apollo_share}-%{apollo_version}
%define apollo_bin   /usr/bin/apollo

Summary:	AcitveMQ's next generation of messaging
Name:		mig-apollo
Version:	%{rpm_version}
Release:	%{rpm_release}%{?dist}
License:	ASL 2.0
Group:		System
Source0:	%{apollo_tar}
Source1:	%{je_jar}
Source3:	service
Source4:	sources
Source5:	99-trunk-version
URL: 		http://activemq.apache.org/apollo/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires:	perl
BuildArch:	noarch
Requires:	java >= 0:1.5.0
Requires:	jpackage-utils

%description
ActiveMQ Apollo is a faster, more reliable, easier to maintain messaging
broker built from the foundations of the original ActiveMQ. It accomplishes
this using a radically different threading and message dispatching
architecture. Like ActiveMQ, Apollo is a multi-protocol broker and supports
STOMP, AMQP, MQTT, Openwire, SSL, and WebSockets.

%prep
if false; then
  echo "je_jar=%{je_jar}"
  echo "apollo_tar=%{apollo_tar}"
  echo "apollo_version=%{apollo_version}"
  echo "is_snapshot=%{is_snapshot}"
  echo "rpm_release=%{rpm_release}"
  echo "rpm_version=%{rpm_version}"
  echo "tar_version=%{tar_version}"
fi
%setup -q -n apache-apollo-%{tar_version}

%build
# optionally add the missing jars
[ "x%{je_jar}" != "xunknown" ] && install -m 0644 %{SOURCE1} lib/
# add our own service script
install -m 0755 %{SOURCE3} bin/

%install
rm -fr %{buildroot}
mkdir -p %{buildroot}%{apollo_home}
mv * %{buildroot}%{apollo_home}

%clean
rm -fr %{buildroot}

%post
[ -e %{apollo_share} ] && rm -f %{apollo_share}
ln -s apollo-%{apollo_version} %{apollo_share}
[ -e %{apollo_bin} ] && rm -f %{apollo_bin}
ln -s %{apollo_share}/bin/apollo %{apollo_bin}

%postun
rm -f %{apollo_share}
cd /usr/share
last=`ls -d apollo-* 2>/dev/null | sort | tail -1`
if [ "x$last" = "x" ]; then
  rm -f %{apollo_bin}
else
  ln -s $last %{apollo_share}
fi

%files
%defattr(-,root,root,-)
%{apollo_home}
